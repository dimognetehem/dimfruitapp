import {Component, Input, OnInit} from '@angular/core';
import {FRUITS} from "../data/data";

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.page.html',
  styleUrls: ['./categorie.page.scss'],
})
export class CategoriePage implements OnInit {


  fruits:any[] = FRUITS;
  categories:string[] = [];
  constructor() {


  }

  ngOnInit() {

    let temp:string[] = [];
    for(let index = 0; index<this.fruits.length-1; index++){
      temp[index] = this.fruits[index].categorie;

    }
    this.categories = temp.filter((x, i) => temp.indexOf(x) === i);

  }

}
