import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsfruitPageRoutingModule } from './detailsfruit-routing.module';

import { DetailsfruitPage } from './detailsfruit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsfruitPageRoutingModule
  ],
  declarations: [DetailsfruitPage]
})
export class DetailsfruitPageModule {}
