import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-detailsfruit',
  templateUrl: './detailsfruit.page.html',
  styleUrls: ['./detailsfruit.page.scss'],
})
export class DetailsfruitPage implements OnInit {
  id:number|any;
  nom:string|any;
  description:string|any;
  prix:string|any;
  categorie:string|any;
  image:string|any;

  constructor(private router : ActivatedRoute) {
    this.id = this.router.snapshot.paramMap.get("id");
    this.nom = this.router.snapshot.paramMap.get("nom");
    this.description = this.router.snapshot.paramMap.get("description");
    this.prix = this.router.snapshot.paramMap.get("prix");
    this.categorie = this.router.snapshot.paramMap.get("categorie");
    this.image = this.router.snapshot.paramMap.get("image");

  }

  ngOnInit() {
  }

}
