import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailsfruitPage } from './detailsfruit.page';

describe('DetailsfruitPage', () => {
  let component: DetailsfruitPage;
  let fixture: ComponentFixture<DetailsfruitPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DetailsfruitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
