import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsfruitPage } from './detailsfruit.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsfruitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsfruitPageRoutingModule {}
