import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListstudentsPage } from './liststudents.page';

describe('ListstudentsPage', () => {
  let component: ListstudentsPage;
  let fixture: ComponentFixture<ListstudentsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListstudentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
