import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListstudentsPageRoutingModule } from './liststudents-routing.module';

import { ListstudentsPage } from './liststudents.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListstudentsPageRoutingModule
  ],
  declarations: [ListstudentsPage]
})
export class ListstudentsPageModule {}
