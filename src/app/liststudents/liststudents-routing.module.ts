import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListstudentsPage } from './liststudents.page';

const routes: Routes = [
  {
    path: '',
    component: ListstudentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListstudentsPageRoutingModule {}
