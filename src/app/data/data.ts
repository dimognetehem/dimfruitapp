
export const FRUITS:any[] = [
  {
    id:1,
    nom:'Cerise',
    description:'La cerise est le fruit comestible du cerisier. Il s\'agit d\'une drupe, de forme sphérique, de couleur généralement rouge plus ou moins foncé jusque noire, plus rarement jaune.',
    prix:500,
    categorie:'Fruit à noyau',
    image:'assets/images/cerise.jpeg'
  },
  {
    id:2,
    nom:'Pomme',
    description:'La pomme est un fruit comestible produit par un pommier. Les pommiers sont cultivés à travers le monde et représentent l\'espèce la plus cultivée du genre Malus.',
    prix:500,
    categorie:'Fruit à pépin',
    image:'assets/images/pomme.jpg'
  },
  {
    id:3,
    nom:'Myrtille',
    description:'Les myrtilles sont les fruits produits par diverses espèces du genre Vaccinium. Ce sont de petites baies de couleur bleu-violacé à la saveur douce et légèrement sucrée.',
    prix:500,
    categorie:'Fruit rouge',
    image:'assets/images/myrtilles.jpg'
  },
  {
    id:4,
    nom:'Orange',
    description:'L\'orange ou orange douce est le fruit de l\'oranger de la famille des Rutacées. Comme pour tous les agrumes, il s\'agit d\'une forme particulière de baie appelée hespéride.',
    prix:500,
    categorie:'Agrume',
    image:'assets/images/orange.jpg'
  },
  {
    id:5,
    nom:'Noix',
    description:'La noix est un fruit comestible à coque. Elle est produite par les noyers, arbres du genre Juglans L., de la famille des Juglandacées.',
    prix:500,
    categorie:'Fruit à coque',
    image:'assets/images/noix.jpg'
  },
  {
    id:6,
    nom:'Ananas',
    description:'Ananas (Ananas comosus). L\'ananas (Ananas comosus) est une espèce de plantes xérophytes, originaire d\'Amérique du Sud, plus spécifiquement du Paraguay, du nord-est de l\'Argentine et sud du Brésil.',
    prix:500,
    categorie:'Fruit exotique',
    image:'assets/images/ananas.jpg'
  },
  {
    id:7,
    nom:'Prune',
    description:'La prune est un fruit à noyau, à chair comestible sucrée et juteuse. Elle est produite par certaines espèces d\'arbres classées dans le genre botanique Prunus.',
    prix:500,
    categorie:'Fruit à noyau',
    image:'assets/images/prune.jpg'
  },
  {
    id:8,
    nom:'Raisin',
    description:'Le raisin est le fruit de la vigne. Le raisin de la vigne cultivée Vitis vinifera est un des fruits les plus cultivés au monde, avec 68 millions de tonnes produites en 2010, derrière les agrumes, les bananes et les pommes.',
    prix:500,
    categorie:'Fruit à pépin',
    image:'assets/images/raisins.png'
  },
  {
    id:9,
    nom:'Cassis',
    description:'Le cassis, est le fruit du cassissier. Cet arbrisseau de la famille des Grossulariacées, originaire de l\'Europe septentrionale et du nord de l\'Asie, pousse spontanément dans les régions montagneuses et froides de la zone paléoarctique',
    prix:500,
    categorie:'Fruit rouge',
    image:'assets/images/cassis.jpg'
  },
  {
    id:10,
    nom:'Citron',
    description:'Le citron est un agrume, fruit du citronnier Citrus limon Burm. f. Il existe des formes douces et acides, le plus commun de nos jours, dont le jus a un pH d\'environ 2,5. Ce fruit, mûr, a une écorce qui va du vert tendre au jaune éclatant sous l\'action du froid.',
    prix:500,
    categorie:'Agrume',
    image:'assets/images/citron.jpg'
  },
  {
    id:11,
    nom:'Amande',
    description:'L\'amande est le fruit de l\'amandier. L’amande est une graine riche en lipide, et en particulier en acides oléique et linoléique, et en oméga-6.',
    prix:500,
    categorie:'Fruit à coque',
    image:'assets/images/amande.jpeg'
  },
  {
    id:12,
    nom:'Mangue',
    description:'La mangue est le fruit du manguier, grand arbre tropical de la famille des Anacardiaceae, originaire des forêts d\'Inde, du Pakistan et de la Birmanie, où il pousse encore à l\'état sauvage.',
    prix:500,
    categorie:'Fruit exotique',
    image:'assets/images/mangue.jpg'
  },
  {
    id:13,
    nom:'Abricot',
    description:'L\'abricot est le fruit d\'un arbre généralement de petite taille appelé abricotier, de la famille des Rosaceae. Le nom scientifique de l\'abricotier est Prunus armeniaca.',
    prix:500,
    categorie:'Fruit à noyau',
    image:'assets/images/abricot.jpg'
  },
  {
    id:14,
    nom:'Clémentine',
    description:'La clémentine est un agrume, fruit du clémentinier, un arbre hybride de la famille des rutacées, issu du croisement entre un mandarinier et un oranger.',
    prix:500,
    categorie:'Agrume',
    image:'assets/images/clementine.jpg'
  },
  {
    id:15,
    nom:'Papaye',
    description:'La papaye est le fruit comestible du papayer, un arbre originaire du Mexique.',
    prix:500,
    categorie:'Fruit exotique',
    image:'assets/images/papaye.jpeg'
  }
];
