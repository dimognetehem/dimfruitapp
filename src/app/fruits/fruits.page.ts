import { Component, OnInit } from '@angular/core';
import {FRUITS} from "../data/data";

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.page.html',
  styleUrls: ['./fruits.page.scss'],
})
export class FruitsPage implements OnInit {

  fruits:any[] = FRUITS;

  constructor() { }

  ngOnInit() {
  }

}
