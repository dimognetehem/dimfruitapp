import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'isj.dimappfruit.ing4isi',
  appName: 'DimFruitApp',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
